var response=JSON.parse(context.getVariable('response.content'));
var payload={
    accessToken:response.access_token,
    scope:response.scope,
    expiresIn:response.expires_in,
    issuedAt:response.issued_at
};
context.setVariable('response.content',JSON.stringify(payload,null,4))
