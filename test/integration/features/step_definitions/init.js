/* jshint node:true */
'use strict';

var fs = require('fs-extra');
var apickli = require('apickli');

var config = require('../../config.json');
var scheme = config.parameters.scheme || 'https';
var domain = config.parameters.domain;
config.parameters.tokenScheme = config.parameters.tokenScheme || 'https';
var basepath = config.parameters.basepath || '/';

if (fs.pathExistsSync('${project.basedir}/target/test/devAppKeys.json'))  {
    console.log("\x1b[32m", "\r> Loading devAppKeys.json");
    var apigee = require('./apigee-init');
    apigee.init(config);
} else {
    console.log("\x1b[33m", "\r> No devAppKeys.json found!");
}

console.log("\x1b[32m", "\r> api parameters: [" + scheme + ', ' + domain + ', ' + basepath + ']');

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        var tags = [];
        scenario.getTags().forEach(tag => {
            tags.push(tag.getName());
        });
        this.apickli = new apickli.Apickli(scheme, domain + basepath);
        this.apickli.addRequestHeader('X-Cucumber-Tags', tags.join(' -> '));
        this.apickli.contexts = config.parameters.contexts;
        Object.keys(config.parameters).forEach(key => {
            this.apickli.storeValueInScenarioScope(key, config.parameters[key]);
        });
        this.apickli.storeValueInScenarioScope('applications', config.applications);
        callback();
    });
};
