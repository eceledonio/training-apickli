'use strict'

var sleep = require('system-sleep');

exports.buildExchangeObj = function (options) {
    var defaultOptions = {
        success: true,
        waitFor: 0,
        response: ""
    };

    Object.assign(defaultOptions, options);

    console.log(defaultOptions);

    var exhange = {
        completed: true,
        isError: function(){
            return defaultOptions.success === false;
        },
        isSuccess: function(){
            return defaultOptions.success === true;
        },
        isComplete: function(){
            return this.completed;
        },
        waitForComplete: async function () {
        },
        getResponse: function() {
            return defaultOptions.response;
        },
        getError: function() {
            return defaultOptions.response;
        },

        // async execute() {
        //     const self = this;
        //     var promise = new Promise(function(resolve, reject) {
        //         console.log('------------------------------ request initiated for ' + defaultOptions.waitFor + ' seconds.');
        //         setTimeout(() => resolve(true), defaultOptions.waitFor);
        //     })
        //     promise.then( (value) => {
        //         console.log('------------------------------ request completed');
        //         self.completed = true;
        //     }, (error) => {
        //         console.log('------------------------------ request error');
        //         console.log(error)
        //     });
        //
        // }
    };
    return exhange;
}
